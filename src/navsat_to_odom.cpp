#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>

#include <GeographicLib/LocalCartesian.hpp>

class NavsatToOdom {
public:
  NavsatToOdom(ros::NodeHandle n) :
    n_(n),
    is_enu_initialized_(false),
    course_(0)
  {
    sub_fix_ = n_.subscribe("fix", 10, &NavsatToOdom::fixCallback, this);
    sub_vel_ = n_.subscribe("vel", 10, &NavsatToOdom::velCallback, this);
    sub_vel_simul_ = n_.subscribe("vel_simul", 10, &NavsatToOdom::velSimulCallback, this);
    pub_odom_ = n_.advertise<nav_msgs::Odometry>("odom", 1);

    ros::NodeHandle n_tilde("~");
    n_tilde.param("publish_tf", publish_tf_, false);
    n_tilde.param<std::string>("frame_parent", frame_parent_, "odom");
    n_tilde.param<std::string>("frame_child", frame_child_, "base_footprint");
    transform_stamped_.header.frame_id = frame_parent_;
    transform_stamped_.child_frame_id = frame_child_;
  }

  void fixCallback(const sensor_msgs::NavSatFix::ConstPtr & fix)
  {
    if(fix->status.status != sensor_msgs::NavSatStatus::STATUS_NO_FIX &&
        fix->latitude == fix->latitude && fix->longitude == fix->longitude) // avoid NaN value
    {
      if(is_enu_initialized_ == false)
      {
        ROS_INFO_STREAM("enu_initialized");
        enu_.Reset(fix->latitude, fix->longitude);
        is_enu_initialized_ = true;
      }
      double east, north, up;
      enu_.Forward(fix->latitude, fix->longitude, 0.0, east, north, up);

      nav_msgs::Odometry odom;
      odom.header.stamp = ros::Time::now();
      odom.header.frame_id = frame_parent_;
      odom.child_frame_id = frame_child_;
      odom.pose.pose.position.x = east;
      odom.pose.pose.position.y = north;
      odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw(course_);
      pub_odom_.publish(odom);
      
      if(publish_tf_ == true) 
      {
        transform_stamped_.header.stamp = ros::Time::now();
        transform_stamped_.transform.translation.x = east;
        transform_stamped_.transform.translation.y = north;
        transform_stamped_.transform.rotation = tf::createQuaternionMsgFromYaw(course_);
        br_.sendTransform(transform_stamped_);
      }
    }
    else
    {
      ROS_WARN_STREAM("Cannot use fix msg : status "<<+fix->status.status<<" ; lat "<<fix->latitude<<" ; lon "<<fix->longitude);
    }
  }

  void velCallback(const geometry_msgs::TwistStamped::ConstPtr & vel)
  {
    double speed = std::sqrt(std::pow(vel->twist.linear.x,2) + std::pow(vel->twist.linear.y,2));
    if(speed > 0.2)
    {
      course_ = atan2(vel->twist.linear.y, vel->twist.linear.x) + M_PI_2;
    }
  }

  void velSimulCallback(const geometry_msgs::Vector3Stamped::ConstPtr & vel)
  {
    double speed = std::sqrt(std::pow(vel->vector.x,2) + std::pow(vel->vector.y,2));
    if(speed > 0.2)
    {
      course_ = atan2(vel->vector.y, vel->vector.x) + M_PI_2;
    }
  }

private:
  ros::NodeHandle n_;
  ros::Subscriber sub_fix_, sub_vel_, sub_vel_simul_;
  ros::Publisher pub_odom_;
  bool publish_tf_;
  std::string frame_parent_, frame_child_;

  GeographicLib::LocalCartesian enu_;
  bool is_enu_initialized_;
  double course_;

  tf2_ros::TransformBroadcaster br_;
  geometry_msgs::TransformStamped transform_stamped_;
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "navsat_to_odom");
  ros::NodeHandle n;
  NavsatToOdom ntoo(n);
  ROS_INFO_STREAM("navsat_to_odom");
  ros::spin();
  return 0;
}
